import numpy as np
from scipy.stats import poisson, chi2
from itertools import compress

def bayesian_chi_squared_test(y, params, distribution='pois', data_type = "continuous", censored = np.nan, chisq_quantile = 0.95):
    n = len(y)
    K = round(n**0.4)
    a = np.linspace(0,1,K+1)
    p = a[1:] - a[:-1]

    if distribution=='pois':
        y_minus_one = [val - 1 for val in y]
        y_densities = [poisson.cdf(y, parameters) for parameters in params]
        y_minus_one_densities = [poisson.cdf(y_minus_one, parameters) for parameters in params]
        probabilities = np.random.uniform(low=y_minus_one_densities, high=y_densities)
    else:
        ValueError('Only exp dist currently supported')
    
    if data_type == "censored":
        if np.isnan(censored):
            ValueError("If applying censored method a vector indicating which data points are censored is required")
        if not len(y) == len(censored):
            ValueError("Length of censored vector be the same length as the data")
        #probabilities = distribution_function(y, x)
            
            # Random sampling from uniform dist:
        #     probs <- probs %>%
        #         dplyr::mutate(g = apply(probs, 1, function(x) {runif(1, min = x[1], max = 1)} )) %>%
        #         dplyr::mutate(censored = censored) %>%
        #         dplyr::mutate(test = ifelse(censored == TRUE, g, probabilities))

        #       return(probs$test)
        #     })
        #   }

    def get_m_vals(x):
        m = [0] * K
        for idx in range(K):
            m[idx] = sum((a[idx] < x) & (x <= a[idx+1]))
        return m
    m = [get_m_vals(x.tolist()) for x in probabilities]

    def get_rb(m, n, p):
        rb = sum([((m[k] - n*p[k])**2)/(n*p[k]) for k in range(len(m))])
        return rb

    r_b = [get_rb(x, n, p) for x in m]

    chi_val = chi2.ppf(chisq_quantile, df=K-1)
    result = sum(r_b > chi_val)/len(r_b) * 100
    
    return result


if __name__=="__main__":
    import pandas as pd
    # bayesian_chi_squared_test(y = supercomputer_failures$failure_count,
    #                            distribution_fun = ppois,
    #                            params = params,
    #                            data_type = "discrete")
    data = {
    'shared_memory_processor_id': [val for val in range(1,48)],
    'failure_count': [1,5,1,4,2,3,1,3,6,4,4,4,2,3,2,2,4,5,5,2,5,3,2,2,3,1,1,2,5,1,4,1,1,1,2,1,3,2,5,3,5,2,5,1,1,5,2]
    }
    supercomputer_failures = pd.DataFrame(data)

    y = supercomputer_failures['failure_count'].tolist()
    params = [2.74318338, 2.96132267, 2.80159604, 3.13496882, 2.71258638, 2.77242275]
    bayesian_chi_squared_test(y, params)